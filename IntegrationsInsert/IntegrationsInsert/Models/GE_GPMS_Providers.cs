﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class GE_GPMS_Providers
    {
        [Display(Name = "GPMS ID (Provider Code)")]
        public string GPMSID { get; set; }
        [Display(Name = "TouchWorks ID (SQL Person ID)")]
        public string TouchworksID { get; set; }
        [Display(Name = "Last Name")]
        public string Lastname { get; set; }
        [Display(Name = "First Name")]
        public string Firstname { get; set; }
    }
}
