﻿using System;
using System.Collections.Generic;

namespace IntegrationsInsert.Models
{
    public partial class DoctorsAudit
    {
        public int RecId { get; set; }
        public decimal ProviderPasnbr { get; set; }
        public string ProviderName { get; set; }
        public string PhysicianGroup { get; set; }
        public string ProviderActive { get; set; }
        public string ProviderUserId { get; set; }
        public string ProviderNpi { get; set; }
        public string ChangeNotes { get; set; }
        public string AuditUser { get; set; }
        public DateTime? AuditTimeStamp { get; set; }
        public string AuditActivity { get; set; }
    }
}
