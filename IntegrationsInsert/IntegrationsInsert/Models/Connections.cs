﻿using System;
using System.Collections.Generic;

namespace IntegrationsInsert.Models
{
    public partial class Connections
    {
        public int ConnectionId { get; set; }
        public byte ConnectionType { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
