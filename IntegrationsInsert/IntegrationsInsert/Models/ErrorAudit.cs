﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class ErrorAudit
    {
        public long ID { get; set; }
        public string Error { get; set; }
        public string AuditUser { get; set; }
        public DateTime ErrorTime { get; set; }
    }
}
