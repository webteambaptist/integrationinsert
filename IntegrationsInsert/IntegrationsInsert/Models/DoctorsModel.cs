﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class DoctorsModel
    {
        public double ProviderPASnbr { get; set; }
        public string ProviderName { get; set; }
        public string PhysicianGroup { get; set; }
        public string ProviderActive { get; set; }
        public string ProviderUserID { get; set; }
        public string ProviderNPI { get; set; }
        public string ChangeNotes { get; set; }
        public bool ToProd { get; set; } 
        public bool ToDev { get; set; }
    }
}
