﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class SuccessModel
    {

        public string SuccessMessage { get; set; }

        public int NumDoctorRecordsDev { get; set; }

        public int NumDoctorRecordsProd { get; set; }

        public int NumGPMSRecordsDev { get; set; }

        public int NumGPMSRecordsProd { get; set; }

        public int NumTouchworksRecordsDev { get; set; }

        public int NumTouchworksRecordsProd { get; set; }
    }
}
