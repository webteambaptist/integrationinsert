﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class TouchworksProvidersModel
    {
        public int TouchworksProvidersID { get; set; }
        public int SQL_Person_ID { get; set; }
        public string NPI { get; set; }
        public string Provider_Code { get; set; }
        public string PAS { get; set; }
        public string User_Type { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime Last_Login { get; set; }
        public string Default_Org { get; set; }
        public string Org_User_Has_Access_To { get; set; }
        public string Org_ID { get; set; }
        public string State_License { get; set; }
        public string State_of_License { get; set; }
        public DateTime State_License_Exp { get; set; }
        public string DEA { get; set; }
        public DateTime DEA_Exp { get; set; }
        public string Default_Location { get; set; }
        public string Profession { get; set; }
        public string Credentials { get; set; }
        public string Specialty { get; set; }
        public string Billing_Provider { get; set; }
        public string Schedulable { get; set; }
        public string Direct_ID { get; set; }
        public string Notes { get; set; }
        public string Active { get; set; }
        public bool ToProd { get; set; }
        public bool ToDev { get; set; }
    }
}
