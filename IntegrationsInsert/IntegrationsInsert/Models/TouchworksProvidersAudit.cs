﻿using System;
using System.Collections.Generic;

namespace IntegrationsInsert.Models
{
    public partial class TouchworksProvidersAudit
    {
        public int RecId { get; set; }
        public int TouchworksProvidersId { get; set; }
        public int? SqlPersonId { get; set; }
        public string Npi { get; set; }
        public string ProviderCode { get; set; }
        public string Pas { get; set; }
        public string UserType { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? LastLogin { get; set; }
        public string DefaultOrg { get; set; }
        public string OrgUserHasAccessTo { get; set; }
        public string OrgId { get; set; }
        public string StateLicense { get; set; }
        public string StateOfLicense { get; set; }
        public DateTime? StateLicenseExp { get; set; }
        public string Dea { get; set; }
        public DateTime? DeaExp { get; set; }
        public string DefaultLocation { get; set; }
        public string Profession { get; set; }
        public string Credentials { get; set; }
        public string Specialty { get; set; }
        public string BillingProvider { get; set; }
        public string Schedulable { get; set; }
        public string DirectId { get; set; }
        public string Notes { get; set; }
        public string Active { get; set; }
        public string AuditUser { get; set; }
        public DateTime? AuditTimeStamp { get; set; }
        public string AuditActivity { get; set; }
    }
}
