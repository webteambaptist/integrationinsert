﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class AddReturnModel
    {
        public bool success;
        public string message;
        public string existMessage;
    }
}
