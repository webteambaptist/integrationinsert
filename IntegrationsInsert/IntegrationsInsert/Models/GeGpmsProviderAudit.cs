﻿using System;
using System.Collections.Generic;

namespace IntegrationsInsert.Models
{
    public partial class GeGpmsProviderAudit
    {
        public int RecId { get; set; }
        public string Gpmsid { get; set; }
        public string TouchWorksId { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string AuditUser { get; set; }
        public DateTime? AuditTimeStamp { get; set; }
        public string AuditActivity { get; set; }
    }
}
