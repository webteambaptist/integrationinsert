﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class TouchworksIDExceptionModel
    {
        public int TouchWorksID { get; set; }

        public Exception touchException { get; set; }

    }
}
