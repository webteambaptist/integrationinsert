﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class GroupedModel
    {
        public Doctors doctors { get; set; }

        public GE_GPMS_Providers gE_GPMS { get; set; }

        public TouchworksProviders touchworks { get; set; }

    }
}
