﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Models
{
    public class ConnectionModel
    {
        public SqlConnection insertConnection { get; set; }

        public SqlConnection insertAudit { get; set; }

        public string environment { get; set; }
    }
}
