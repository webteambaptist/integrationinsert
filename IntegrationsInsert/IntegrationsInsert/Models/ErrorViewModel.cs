using System;
using System.Collections.Generic;

namespace IntegrationsInsert.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        public string exOutputMessage { get; set; }

        public bool ToProd { get; set; }

        public string TableToBeInserted { get; set; }

        public int LineNumber { get; set; }

        public int NumDoctorRecordsDev { get; set; }

        public int NumDoctorRecordsProd { get; set; }

        public int NumGPMSRecordsDev { get; set; }

        public int NumGPMSRecordsProd { get; set; }

        public int NumTouchworksRecordsDev { get; set; }

        public int NumTouchworksRecordsProd { get; set; }
        public List<string> errors { get; set; }
    }
}