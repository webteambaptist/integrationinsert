﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IntegrationsInsert.Models
{
    public partial class TouchworksProviders
    {
        public int TouchworksProvidersId { get; set; }
        [Display(Name = "SQL Person ID")]
        public int? SqlPersonId { get; set; }
        [Display(Name = "NPI")]
        public string Npi { get; set; }
        [Display(Name = "Provider Code")]
        public string ProviderCode { get; set; }
        [Display(Name = "PAS")]
        public string Pas { get; set; }
        [Display(Name = "User Type")]
        public string UserType { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Login")]
        public DateTime? LastLogin { get; set; }
        [Display(Name = "Default Org")]
        public string DefaultOrg { get; set; }
        [Display(Name = "Org User Has Access To")]
        public string OrgUserHasAccessTo { get; set; }
        [Display(Name = "Org ID")]
        public string OrgId { get; set; }
        [Display(Name = "State License")]
        public string StateLicense { get; set; }
        [Display(Name = "State of License")]
        public string StateOfLicense { get; set; }
        [Display(Name = "State License Exp")]
        public DateTime? StateLicenseExp { get; set; }
        [Display(Name = "DEA")]
        public string Dea { get; set; }
        [Display(Name = "DEA Exp")]
        public DateTime? DeaExp { get; set; }
        [Display(Name = "Default Location")]
        public string DefaultLocation { get; set; }
        [Display(Name = "Profession")]
        public string Profession { get; set; }
        [Display(Name = "Credentials")]
        public string Credentials { get; set; }
        [Display(Name = "Specialty")]
        public string Specialty { get; set; }
        [Display(Name = "Billing Provider")]
        public string BillingProvider { get; set; }
        [Display(Name = "Schedulable")]
        public string Schedulable { get; set; }
        [Display(Name = "Direct ID")]
        public string DirectId { get; set; }
        [Display(Name = "Notes")]
        public string Notes { get; set; }
        [Display(Name = "Active")]
        public string Active { get; set; }
    }
}
