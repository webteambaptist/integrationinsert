﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IntegrationsInsert.Models
{
    public partial class Doctors
    {
        [Display(Name = "Provider PAS NBR")]
        public decimal ProviderPasnbr { get; set; }
        [Display(Name = "Provider Name (LastName, FirstName)")]
        public string ProviderName { get; set; }
        [Display(Name = "Physician Group")] 
        public string PhysicianGroup { get; set; }
        [Display(Name = "Provider Active")]
        public string ProviderActive { get; set; }
        [Display(Name = "Provider User Id")]
        public string ProviderUserId { get; set; }
        [Display(Name = "Provider NPI")]
        public string ProviderNpi { get; set; }
        [Display(Name = "Change Notes (Date of change and User Id)")]
        public string ChangeNotes { get; set; }
    }
}
