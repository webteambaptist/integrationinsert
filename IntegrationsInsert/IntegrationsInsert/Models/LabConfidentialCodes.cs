﻿using System;
using System.Collections.Generic;

namespace IntegrationsInsert.Models
{
    public partial class LabConfidentialCodes
    {
        public string LabCode { get; set; }
        public string LabDescription { get; set; }
    }
}
