﻿$(document).ready(function () {
    if ($('#file').get(0).files.length === 0) {
        $('#btnUpload').attr('disabled', 'disabled');
        $('#btnUpload').removeClass('btn btn-primary');
    }
});

$('input[type=file]').change(function () {
    if ($('#file').get(0).files.length === 0) {
        $('#btnUpload').attr('disabled', 'disabled');
        $('#btnUpload').removeClass('btn btn-primary');
    }
    else {
        $('#btnUpload').removeAttr('disabled');
        $('#btnUpload').addClass('btn btn-primary');
    }
});