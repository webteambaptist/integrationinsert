﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IntegrationsInsert.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Principal;
using Microsoft.AspNetCore.Authentication;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace IntegrationsInsert.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public IActionResult Index(SuccessModel model)
        {

            var wi = (WindowsIdentity)User.Identity;

            PrincipalContext pc = new PrincipalContext(ContextType.Domain, "BH");
            var src = UserPrincipal.FindByIdentity(pc, User.Identity.Name).GetGroups(pc);


            var groups = src.Select(x => x).Select(x => x.Name).Where(y => y.Contains("BHSQL Corepoint Integrations Users")).ToList();


            if (groups.Any())
            {
                ViewData["ShowInsert"] = true;
            }
            else
            {
                ViewData["ShowInsert"] = false;
            }

            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(Exception ex)
        {
            return View(new ErrorViewModel { RequestId = Activity.Current.Id ?? HttpContext.TraceIdentifier, exOutputMessage = ex.Message });
        }
    }
}
