﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntegrationsInsert.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Data;
using OfficeOpenXml;
using System.Transactions;
using Microsoft.Extensions.Configuration;
using IntegrationsInsert.data;
using IntegrationsInsert.Utils;
//using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Text;
using NLog;

namespace IntegrationsInsert.Controllers
{
    public class IntegrationsController : Controller
    {
        public IConfiguration _config;
        private readonly DevContext _devContext;
        private readonly DevAuditContext _devAuditContext;
        private readonly ProdContext _prodContext;
        private readonly ProdAuditContext _prodAuditContext;
        private readonly string DeployedEnv;
        //private readonly string LoggingPath;
        //private readonly ILogger _logger;
        private static readonly Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private List<string> errors = new List<string>();
        public IntegrationsController(IConfiguration c, DevContext dContext, DevAuditContext dAuditContext, ProdContext pContext, ProdAuditContext pAuditContext)
        {
            _config = c;
            _devContext = dContext;
            _devAuditContext = dAuditContext;
            _prodContext = pContext;
            _prodAuditContext = pAuditContext;
            //_logger = logger.CreateLogger("IntegrationInsert.IntegrationInsert.IntegrationsController");
            DeployedEnv = _config.GetSection("DeployedEnvironment").Value;
            //LoggingPath = _config.GetSection("logPath").Value;

            var config = new NLog.Config.LoggingConfiguration();

            var orFile = new NLog.Targets.FileTarget("logfile") { FileName = "IntegrationsInsert.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, orFile);
            LogManager.Configuration = config;
        }
        [Authorize]
        public IActionResult Insert()
        {
            var name = HttpContext.User.Identity.Name;


            return View();
        }
        [Authorize]
        public IActionResult InsertToDoctors()
        {
            var name = HttpContext.User.Identity.Name;
            Doctors model = new Doctors();

            return View(model);
        }
        [Authorize]
        public IActionResult InsertToGPSMProviders()
        {
            var name = HttpContext.User.Identity.Name;
            GE_GPMS_Providers model = new GE_GPMS_Providers();

            return View(model);
        }
        [Authorize]
        public IActionResult InsertToTouchworks()
        {
            var name = HttpContext.User.Identity.Name;
            TouchworksProviders model = new TouchworksProviders();

            return View(model);
        }
        [Authorize]
        public IActionResult Error(ErrorViewModel model)
        {
            return View(model);
        }
        public void ErrorAudit(ErrorAudit audit)
        {
            bool devsuccessful = false;
            if (DeployedEnv == "Dev")
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        ErrorAudit e = new ErrorAudit();
                        e.AuditUser = audit.AuditUser;
                        e.Error = audit.Error;
                        e.ErrorTime = audit.ErrorTime;
                    _devAuditContext.ErrorAudit.Add(e);
                    _devAuditContext.SaveChanges();

                        t.Commit();
                    }
                    catch (Exception e)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Error Audit:(Dev) " + audit.Error, LoggingPath);
                        _logger.Info("Exception occurred while adding Error Audit:(Dev) " + audit.Error);
                    }
                }
            }
            else
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        ErrorAudit e = new ErrorAudit();
                        e.AuditUser = audit.AuditUser;
                        e.Error = audit.Error;
                        e.ErrorTime = audit.ErrorTime;
                    _devAuditContext.ErrorAudit.Add(e);
                    _devAuditContext.SaveChanges();

                        t.Commit();
                        devsuccessful = true;
                    }
                    catch (Exception e)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Error Audit:(Dev) " + audit.Error, LoggingPath);
                        _logger.Info("Exception occurred while adding Error Audit:(Dev) " + audit.Error);
                    }
                }
                if (devsuccessful)
                {
                    using (var t = _prodAuditContext.Database.BeginTransaction())
                    {
                        try
                        {
                            ErrorAudit e = new ErrorAudit();
                            e.AuditUser = audit.AuditUser;
                            e.Error = audit.Error;
                            e.ErrorTime = audit.ErrorTime;
                        _prodAuditContext.ErrorAudit.Add(e);
                        _prodAuditContext.SaveChanges();

                            t.Commit();
                        }
                        catch (Exception e)
                        {
                            t.Rollback();
                            //Logging.LogMessage("Exception occurred while adding Error Audit:(Prod) " + audit.Error, LoggingPath);
                            _logger.Info("Exception occurred while adding Error Audit:(Prod) " + audit.Error);
                        }
                    }
                }
            }
        }
        public bool DoctorAudit(Doctors model)
        {
            if (DeployedEnv == "Dev")
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        DoctorsAudit audit = new DoctorsAudit();
                        audit.ChangeNotes = model.ChangeNotes;
                        audit.PhysicianGroup = model.PhysicianGroup;
                        audit.ProviderActive = model.ProviderActive;
                        audit.ProviderName = model.ProviderName;
                        audit.ProviderNpi = model.ProviderNpi;
                        audit.ProviderPasnbr = model.ProviderPasnbr;
                        audit.ProviderUserId = model.ProviderUserId;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;

                    _devAuditContext.DoctorsAudit.Add(audit);
                    _devAuditContext.SaveChanges();


                        t.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Doctors Audit (Dev): " + model.ProviderName, LoggingPath);
                        _logger.Info("Exception occurred while adding Doctors Audit (Dev): " + model.ProviderName);

                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
                }
            }
            else
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        DoctorsAudit audit = new DoctorsAudit();
                        audit.ChangeNotes = model.ChangeNotes;
                        audit.PhysicianGroup = model.PhysicianGroup;
                        audit.ProviderActive = model.ProviderActive;
                        audit.ProviderName = model.ProviderName;
                        audit.ProviderNpi = model.ProviderNpi;
                        audit.ProviderPasnbr = model.ProviderPasnbr;
                        audit.ProviderUserId = model.ProviderUserId;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;

                        _devAuditContext.DoctorsAudit.Add(audit);
                        _devAuditContext.SaveChanges();


                        t.Commit();
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Doctors Audit:(Dev) " + model.ProviderName, LoggingPath);
                        _logger.Info("Exception occurred while adding Doctors Audit:(Dev) " + model.ProviderName);

                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
                }
                using (var t = _prodAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        DoctorsAudit audit = new DoctorsAudit();
                        audit.ChangeNotes = model.ChangeNotes;
                        audit.PhysicianGroup = model.PhysicianGroup;
                        audit.ProviderActive = model.ProviderActive;
                        audit.ProviderName = model.ProviderName;
                        audit.ProviderNpi = model.ProviderNpi;
                        audit.ProviderPasnbr = model.ProviderPasnbr;
                        audit.ProviderUserId = model.ProviderUserId;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;

                    _prodAuditContext.DoctorsAudit.Add(audit);
                    _prodAuditContext.SaveChanges();


                        t.Commit();
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Doctors Audit:(Prod) " + model.ProviderName, LoggingPath);
                        _logger.Info("Exception occurred while adding Doctors Audit:(Prod) " + model.ProviderName);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
                }
                return true;
            }
        }
        public bool TouchworksProviderAudit(TouchworksProviders model)
        {
            if (DeployedEnv == "Dev")
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        TouchworksProvidersAudit audit = new TouchworksProvidersAudit();
                        audit.Active = model.Active;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;
                        audit.BillingProvider = model.BillingProvider;
                        audit.Credentials = model.Credentials;
                        audit.Dea = model.Dea;
                        audit.DeaExp = model.DeaExp;
                        audit.DefaultLocation = model.DefaultLocation;
                        audit.DefaultOrg = model.DefaultOrg;
                        audit.DirectId = model.DirectId;
                        audit.FirstName = model.FirstName;
                        audit.LastLogin = model.LastLogin;
                        audit.LastName = model.LastName;
                        audit.Notes = model.Notes;
                        audit.Npi = model.Npi;
                        audit.OrgId = model.OrgId;
                        audit.OrgUserHasAccessTo = model.OrgUserHasAccessTo;
                        audit.Pas = model.Pas;
                        audit.Profession = model.Profession;
                        audit.ProviderCode = model.ProviderCode;
                        audit.Schedulable = model.Schedulable;
                        audit.Specialty = model.Specialty;
                        audit.SqlPersonId = model.SqlPersonId;
                        audit.StateLicense = model.StateLicense;
                        audit.StateLicenseExp = model.StateLicenseExp;
                        audit.StateOfLicense = model.StateOfLicense;
                        audit.TouchworksProvidersId = model.TouchworksProvidersId;
                        audit.UserType = model.UserType;

                    _devAuditContext.TouchworksProvidersAudit.Add(audit);
                    _devAuditContext.SaveChanges();

                        t.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Touchworks Provider:(Dev) " + model.FirstName + " " + model.LastName, LoggingPath);
                        _logger.Info("Exception occurred while adding Touchworks Provider:(Dev) " + model.FirstName + " " + model.LastName);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
                }
            }
            else
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        TouchworksProvidersAudit audit = new TouchworksProvidersAudit();
                        audit.Active = model.Active;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;
                        audit.BillingProvider = model.BillingProvider;
                        audit.Credentials = model.Credentials;
                        audit.Dea = model.Dea;
                        audit.DeaExp = model.DeaExp;
                        audit.DefaultLocation = model.DefaultLocation;
                        audit.DefaultOrg = model.DefaultOrg;
                        audit.DirectId = model.DirectId;
                        audit.FirstName = model.FirstName;
                        audit.LastLogin = model.LastLogin;
                        audit.LastName = model.LastName;
                        audit.Notes = model.Notes;
                        audit.Npi = model.Npi;
                        audit.OrgId = model.OrgId;
                        audit.OrgUserHasAccessTo = model.OrgUserHasAccessTo;
                        audit.Pas = model.Pas;
                        audit.Profession = model.Profession;
                        audit.ProviderCode = model.ProviderCode;
                        audit.Schedulable = model.Schedulable;
                        audit.Specialty = model.Specialty;
                        audit.SqlPersonId = model.SqlPersonId;
                        audit.StateLicense = model.StateLicense;
                        audit.StateLicenseExp = model.StateLicenseExp;
                        audit.StateOfLicense = model.StateOfLicense;
                        audit.TouchworksProvidersId = model.TouchworksProvidersId;
                        audit.UserType = model.UserType;

                    _devAuditContext.TouchworksProvidersAudit.Add(audit);
                    _devAuditContext.SaveChanges();

                        t.Commit();
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Touchworks Provider:(Dev) " + model.FirstName + " " + model.LastName, LoggingPath);
                        _logger.Info("Exception occurred while adding Touchworks Provider:(Dev) " + model.FirstName + " " + model.LastName);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
                }
                using (var t = _prodAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        TouchworksProvidersAudit audit = new TouchworksProvidersAudit();
                        audit.Active = model.Active;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;
                        audit.BillingProvider = model.BillingProvider;
                        audit.Credentials = model.Credentials;
                        audit.Dea = model.Dea;
                        audit.DeaExp = model.DeaExp;
                        audit.DefaultLocation = model.DefaultLocation;
                        audit.DefaultOrg = model.DefaultOrg;
                        audit.DirectId = model.DirectId;
                        audit.FirstName = model.FirstName;
                        audit.LastLogin = model.LastLogin;
                        audit.LastName = model.LastName;
                        audit.Notes = model.Notes;
                        audit.Npi = model.Npi;
                        audit.OrgId = model.OrgId;
                        audit.OrgUserHasAccessTo = model.OrgUserHasAccessTo;
                        audit.Pas = model.Pas;
                        audit.Profession = model.Profession;
                        audit.ProviderCode = model.ProviderCode;
                        audit.Schedulable = model.Schedulable;
                        audit.Specialty = model.Specialty;
                        audit.SqlPersonId = model.SqlPersonId;
                        audit.StateLicense = model.StateLicense;
                        audit.StateLicenseExp = model.StateLicenseExp;
                        audit.StateOfLicense = model.StateOfLicense;
                        audit.TouchworksProvidersId = model.TouchworksProvidersId;
                        audit.UserType = model.UserType;

                    _prodAuditContext.TouchworksProvidersAudit.Add(audit);
                    _prodAuditContext.SaveChanges();


                        t.Commit();
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding Touchworks Provider:(Prod) " + model.FirstName + model.LastName, LoggingPath);
                        _logger.Info("Exception occurred while adding Touchworks Provider:(Prod) " + model.FirstName + model.LastName);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
                }
                return true;
            }
        }
        public bool GEGPMSProvidersAudit(GE_GPMS_Providers model)
        {
            bool devsuccessful = false;
            if (DeployedEnv == "Dev")
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        GeGpmsProviderAudit audit = new GeGpmsProviderAudit();
                        audit.Firstname = model.Firstname;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;
                        audit.Gpmsid = model.GPMSID;
                        audit.Lastname = model.Lastname;
                        audit.TouchWorksId = model.TouchworksID;

                    _devAuditContext.GeGpmsProviderAudit.Add(audit);
                    _devAuditContext.SaveChanges();

                        t.Commit();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding GE-GPMS Provider:(Dev) " + model.Firstname + " " + model.Lastname, LoggingPath);
                        _logger.Info("Exception occurred while adding GE-GPMS Provider:(Dev) " + model.Firstname + " " + model.Lastname);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
                }
            }
            else
            {
                using (var t = _devAuditContext.Database.BeginTransaction())
                {
                    try
                    {
                        GeGpmsProviderAudit audit = new GeGpmsProviderAudit();
                        audit.Firstname = model.Firstname;
                        audit.AuditActivity = "I";
                        audit.AuditTimeStamp = DateTime.Now;
                        audit.AuditUser = User.Identity.Name;
                        audit.Gpmsid = model.GPMSID;
                        audit.Lastname = model.Lastname;
                        audit.TouchWorksId = model.TouchworksID;

                    _devAuditContext.GeGpmsProviderAudit.Add(audit);
                    _devAuditContext.SaveChanges();

                        t.Commit();
                    devsuccessful = true;
                    }
                    catch (Exception ex)
                    {
                        t.Rollback();
                        //Logging.LogMessage("Exception occurred while adding GE_GPMS Provider:(Dev) " + model.Firstname + " " + model.Lastname, LoggingPath);
                        _logger.Info("Exception occurred while adding GE_GPMS Provider:(Dev) " + model.Firstname + " " + model.Lastname);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                        return false;
                    }
            }
                if (devsuccessful)
                {
                    using (var t = _prodAuditContext.Database.BeginTransaction())
                    {
                        try
                        {
                            GeGpmsProviderAudit audit = new GeGpmsProviderAudit();
                            audit.Firstname = model.Firstname;
                            audit.AuditActivity = "I";
                            audit.AuditTimeStamp = DateTime.Now;
                            audit.AuditUser = User.Identity.Name;
                            audit.Gpmsid = model.GPMSID;
                            audit.Lastname = model.Lastname;
                            audit.TouchWorksId = model.TouchworksID;

                        _prodAuditContext.GeGpmsProviderAudit.Add(audit);
                        _prodAuditContext.SaveChanges();

                            t.Commit();
                        }
                        catch (Exception ex)
                        {
                            t.Rollback();
                            //Logging.LogMessage("Exception occurred while adding GE_GPMS Provider:(Prod) " + model.Firstname + " " + model.Lastname, LoggingPath);
                            _logger.Info("Exception occurred while adding GE_GPMS Provider:(Prod) " + model.Firstname + " " + model.Lastname);
                            ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = ex.Message.ToString(), ErrorTime = DateTime.Now });
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        public AddReturnModel AddDoctorDev(Doctors model)
        {
            AddReturnModel _model = new AddReturnModel();
            _model.success = true;
            using (var transaction = _devContext.Database.BeginTransaction())
            {
                bool exists = _devContext.Doctors.Where(x => x.ProviderPasnbr == model.ProviderPasnbr && x.PhysicianGroup == model.PhysicianGroup).Any();
                if (exists)
                {
                    _model.existMessage = "Doctor already exists: NO Add Performed on (Dev) " + model.ProviderName;
                    // Logging.LogMessage(_model.existMessage, LoggingPath);
                    _logger.Info(_model.existMessage);
                    ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = _model.existMessage, ErrorTime = DateTime.Now });
                }
                else
                {
                    try
                    {
                        Doctors doc = new Doctors();
                        doc.ChangeNotes = model.ChangeNotes;
                        doc.PhysicianGroup = model.PhysicianGroup;
                        doc.ProviderActive = model.ProviderActive;
                        doc.ProviderName = model.ProviderName;
                        doc.ProviderNpi = model.ProviderNpi;
                        doc.ProviderPasnbr = model.ProviderPasnbr;
                        doc.ProviderUserId = model.ProviderUserId;

                    _devContext.Doctors.Add(doc);
                    _devContext.SaveChanges();

                        bool auditsucceed = DoctorAudit(model);

                        if (auditsucceed)
                        {
                            _model.success = true;
                            _model.message = "Insert Completed Successfully for Doctor " + model.ProviderName;


                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                        }
                        else
                        {
                            transaction.Rollback();
                            _model.success = false;
                            _model.message = "Doctor " + model.ProviderName + " not added see logs";
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                            ErrorAudit(new ErrorAudit { AuditUser = User.Identity.Name, Error = "Doctor " + model.ProviderName + " not added", ErrorTime = DateTime.Now });
                            return _model;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _model.success = false;
                        _model.message = "Exception occurred while adding Doctors: " + model.ProviderName + " " + ex.Message;
                        //Logging.LogMessage(_model.message, LoggingPath);
                        _logger.Info(_model.message);
                        ErrorAudit(new ErrorAudit { AuditUser = User.Identity.Name, Error = "Doctor " + model.ProviderName + " not added see logs", ErrorTime = DateTime.Now });
                        return _model;
                    }
                }
                transaction.Commit(); // only commit if no errors occurred
            }
            return _model;
        }
        public AddReturnModel AddDoctorProd(Doctors model)
        {
            AddReturnModel _model = new AddReturnModel();
            _model.success = true;
            using (var transaction = _prodContext.Database.BeginTransaction())
            {
                bool exists = _prodContext.Doctors.Where(x => x.ProviderPasnbr == model.ProviderPasnbr && x.PhysicianGroup == model.PhysicianGroup).Any();
                if (exists)
                {
                    _model.existMessage = "Doctor already exists: NO Add Performed on " + model.ProviderName;
                    //Logging.LogMessage(_model.existMessage, LoggingPath);
                    _logger.Info(_model.existMessage);
                    ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = _model.existMessage, ErrorTime = DateTime.Now });
                    return _model;
                }
                else
                {
                    try
                    {
                        Doctors doc = new Doctors();
                        doc.ChangeNotes = model.ChangeNotes;
                        doc.PhysicianGroup = model.PhysicianGroup;
                        doc.ProviderActive = model.ProviderActive;
                        doc.ProviderName = model.ProviderName;
                        doc.ProviderNpi = model.ProviderNpi;
                        doc.ProviderPasnbr = model.ProviderPasnbr;
                        doc.ProviderUserId = model.ProviderUserId;

                        _prodContext.Doctors.Add(doc);
                        _prodContext.SaveChanges();

                        bool auditsucceed = DoctorAudit(model);

                        if (auditsucceed)
                        {
                            _model.message = "Insert Completed Successfully for Doctor " + model.ProviderName;
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                        }
                        else
                        {
                            transaction.Rollback();
                            _model.success = false;
                            _model.message = "Doctor " + model.ProviderName + " not added";
                            _logger.Info(_model.message);
                            ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = _model.message, ErrorTime = DateTime.Now });
                            return _model;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _model.success = false;
                        _model.message = "Exception occurred while adding Doctors: (Prod) " + model.ProviderName + " " + ex.Message;
                        //Logging.LogMessage(_model.message, LoggingPath);
                        _logger.Info(_model.message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = _model.message, ErrorTime = DateTime.Now });
                        return _model;
                    }
                }
                transaction.Commit(); // only commit if no errors occurred
                return _model;
            }
            
        }
        public AddReturnModel AddTouchworksDev(TouchworksProviders model)
        {
            AddReturnModel _model = new AddReturnModel();
            _model.success = true;
            using (var transaction = _devContext.Database.BeginTransaction())
            {
                bool exists = _devContext.TouchworksProviders.Where(x => x.SqlPersonId == model.SqlPersonId 
                && x.ProviderCode == model.ProviderCode).Any();
                if (exists)
                {
                    _model.existMessage = "Touchworks Provider already exists (Dev): NO Add Performed on " + model.FirstName + " " + model.LastName;
                    //Logging.LogMessage(_model.existMessage, LoggingPath);
                    _logger.Info(_model.existMessage);
                    ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                        Error = _model.existMessage, ErrorTime = DateTime.Now });
                    return _model;
                }
                else
                {
                    try
                    {
                        TouchworksProviders provider = new TouchworksProviders();
                        provider.Active = model.Active;
                        provider.BillingProvider = model.BillingProvider;
                        provider.Credentials = model.Credentials;
                        provider.Dea = model.Dea;
                        provider.DeaExp = model.DeaExp;
                        provider.DefaultLocation = model.DefaultLocation;
                        provider.DefaultOrg = model.DefaultOrg;
                        provider.DirectId = model.DirectId;
                        provider.FirstName = model.FirstName;
                        provider.LastLogin = model.LastLogin;
                        provider.LastName = model.LastName;
                        provider.Notes = model.Notes;
                        provider.Npi = model.Npi;
                        provider.OrgId = model.OrgId;
                        provider.OrgUserHasAccessTo = model.OrgUserHasAccessTo;
                        provider.Pas = model.Pas;
                        provider.Profession = model.Profession;
                        provider.ProviderCode = model.ProviderCode;
                        provider.Schedulable = model.Schedulable;
                        provider.Specialty = model.Specialty;
                        provider.SqlPersonId = model.SqlPersonId;
                        provider.StateLicense = model.StateLicense;
                        provider.StateLicenseExp = model.StateLicenseExp;
                        provider.StateOfLicense = model.StateOfLicense;
                        provider.UserType = model.UserType;

                    _devContext.TouchworksProviders.Add(provider);
                    _devContext.SaveChanges();

                        bool auditsucceed = TouchworksProviderAudit(model);

                        if (auditsucceed)
                        {
                            _model.message = "Insert Completed Successfully for Touchworks Provider (Dev) " + model.FirstName + " " + model.LastName;
                            // Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                        }
                        else
                        {
                            transaction.Rollback();
                            _model.success = false;
                            _model.message = "(Dev) Touchworks Provider " + model.FirstName + " " + model.LastName + " not added see logs";
                            // Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                            ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                                Error = _model.message, ErrorTime = DateTime.Now });
                            return _model;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _model.success = false;
                        _model.message = "Exception occurred while adding Touchworks Provider: (Dev) " + model.FirstName + " " + model.LastName + " " + ex.Message;
                        //Logging.LogMessage(_model.message, LoggingPath);
                        _logger.Info(_model.message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                            Error = _model.message, ErrorTime = DateTime.Now });
                        return _model;
                    }
                }
                transaction.Commit(); // only commit if no errors occurred
                return _model;
            }
        }
        public AddReturnModel AddTouchworksProd(TouchworksProviders model)
        {
            AddReturnModel _model = new AddReturnModel();
            _model.success = true;
            using (var transaction = _prodContext.Database.BeginTransaction())
            {
                bool exists = _prodContext.TouchworksProviders.Where(x => x.Npi == model.Npi).Any();
                if (exists)
                {
                    _model.existMessage = "(Prod) Touchworks Provider already exists: NO Add Performed on " + model.FirstName + " " + model.LastName;
                    //Logging.LogMessage(_model.existMessage, LoggingPath);
                    _logger.Info(_model.existMessage);
                    ErrorAudit(new Models.ErrorAudit
                    {
                        AuditUser = User.Identity.Name,
                        Error = _model.existMessage,
                        ErrorTime = DateTime.Now
                    });
                    return _model;
                }
                else
                {
                    try
                    {
                        TouchworksProviders provider = new TouchworksProviders();
                        provider.Active = model.Active;
                        provider.BillingProvider = model.BillingProvider;
                        provider.Credentials = model.Credentials;
                        provider.Dea = model.Dea;
                        provider.DeaExp = model.DeaExp;
                        provider.DefaultLocation = model.DefaultLocation;
                        provider.DefaultOrg = model.DefaultOrg;
                        provider.DirectId = model.DirectId;
                        provider.FirstName = model.FirstName;
                        provider.LastLogin = model.LastLogin;
                        provider.LastName = model.LastName;
                        provider.Notes = model.Notes;
                        provider.Npi = model.Npi;
                        provider.OrgId = model.OrgId;
                        provider.OrgUserHasAccessTo = model.OrgUserHasAccessTo;
                        provider.Pas = model.Pas;
                        provider.Profession = model.Profession;
                        provider.ProviderCode = model.ProviderCode;
                        provider.Schedulable = model.Schedulable;
                        provider.Specialty = model.Specialty;
                        provider.SqlPersonId = model.SqlPersonId;
                        provider.StateLicense = model.StateLicense;
                        provider.StateLicenseExp = model.StateLicenseExp;
                        provider.StateOfLicense = model.StateOfLicense;
                        provider.UserType = model.UserType;

                        _prodContext.TouchworksProviders.Add(provider);
                        _prodContext.SaveChanges();

                        bool auditsucceed = TouchworksProviderAudit(model);

                        if (auditsucceed)
                        {
                            _model.message = "Insert Completed Successfully for Touchworks Provider (Prod) " + model.FirstName + " " + model.LastName;
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                        }
                        else
                        {
                            transaction.Rollback();
                            _model.success = false;
                            _model.message = "Touchworks Provider (Prod)" + model.FirstName + " " + model.LastName + " not added see logs";
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                            ErrorAudit(new Models.ErrorAudit
                            {
                                AuditUser = User.Identity.Name,
                                Error = "Touchworks Provider " + model.FirstName + " " + model.LastName + " not added",
                                ErrorTime = DateTime.Now
                            });
                            return _model;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _model.success = false;
                        _model.message = "Exception occurred while adding Touchworks Provider: (Prod)" + model.FirstName + " " + model.LastName + " " + ex.Message;
                        //Logging.LogMessage(_model.message, LoggingPath);
                        _logger.Info(_model.message);
                        ErrorAudit(new Models.ErrorAudit
                        {
                            AuditUser = User.Identity.Name,
                            Error = _model.message,
                            ErrorTime = DateTime.Now
                        });
                        return _model;
                    }
                }
                transaction.Commit(); // only commit if no errors occurred
                return _model;
            }
        }
        public AddReturnModel AddGPSMProviderDev(GE_GPMS_Providers model)
        {
            AddReturnModel _model = new AddReturnModel();
            _model.success = true;
            using (var transaction = _devContext.Database.BeginTransaction())
            {
                bool exists = _devContext.GE_GPMS_Providers.Where(x => x.GPMSID == model.GPMSID && x.TouchworksID == model.TouchworksID && 
                x.Lastname == model.Lastname && x.Firstname == model.Firstname).Any();
                if (exists)
                {
                    _model.existMessage = "GE_CPMS_Provider already exists: Not Added (Dev) " + model.Firstname + " " + model.Lastname;
                    //Logging.LogMessage(_model.existMessage, LoggingPath);
                    _logger.Info(_model.existMessage);
                    ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                        Error = _model.existMessage, ErrorTime = DateTime.Now });
                    return _model;
                }
                else
                {
                    try
                    {
                        GE_GPMS_Providers providers = new GE_GPMS_Providers();
                        providers.Firstname = model.Firstname;
                        providers.Lastname = model.Lastname;
                        providers.GPMSID = model.GPMSID;
                        providers.TouchworksID = model.TouchworksID;

                    _devContext.GE_GPMS_Providers.Add(providers);
                    _devContext.SaveChanges();

                        bool auditsucceed = GEGPMSProvidersAudit(model);

                        if (auditsucceed)
                        {
                            _model.message = "Insert Completed Successfully for GE_GPMS_Provider (Dev) " + model.Firstname + " " + model.Lastname;
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                        }
                        else
                        {
                            transaction.Rollback();
                            _model.success = false;
                            _model.message = "(Dev) Doctor " + model.Firstname + " " + model.Lastname + " not added see logs";
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                            ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                                Error = _model.message, ErrorTime = DateTime.Now });
                            return _model;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _model.success = false;
                        _model.message = "Exception occurred while adding GE_CPMS_Provider (Dev): " + model.Firstname + " " + model.Lastname + " " + ex.Message;
                        //Logging.LogMessage(_model.message, LoggingPath);
                        _logger.Info(_model.message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                            Error = _model.message, ErrorTime = DateTime.Now });
                        return _model;
                    }
                }
                transaction.Commit(); // only commit if no errors occurred
                return _model;
            }
        }
        public AddReturnModel AddGPSMProviderProd(GE_GPMS_Providers model)
        {
            AddReturnModel _model = new AddReturnModel();
            _model.success = true;
            using (var transaction = _prodContext.Database.BeginTransaction())
            {
                bool exists = _prodContext.GE_GPMS_Providers.Where(x => x.GPMSID == model.GPMSID && x.TouchworksID == model.TouchworksID &&
                x.Lastname == model.Lastname && x.Firstname == model.Firstname).Any();
                if (exists)
                {
                    _model.existMessage = "GE_CPMS_Provider already exists (Prod): Not Added " + model.Firstname + " " + model.Lastname;
                    //Logging.LogMessage(_model.existMessage, LoggingPath);
                    _logger.Info(_model.existMessage);
                    ErrorAudit(new Models.ErrorAudit
                    {
                        AuditUser = User.Identity.Name,
                        Error = "GE_GPMS_Providers (Prod) " + model.Firstname + " " + model.Lastname + " Already Exists",
                        ErrorTime = DateTime.Now
                    });
                    return _model;
                }
                else
                {
                    try
                    {
                        GE_GPMS_Providers providers = new GE_GPMS_Providers();
                        providers.Firstname = model.Firstname;
                        providers.Lastname = model.Lastname;
                        providers.GPMSID = model.GPMSID;
                        providers.TouchworksID = model.TouchworksID;

                        _prodContext.GE_GPMS_Providers.Add(providers);
                        _prodContext.SaveChanges();

                        bool auditsucceed = GEGPMSProvidersAudit(model);

                        if (auditsucceed)
                        {
                            _model.message = "Insert Completed Successfully for GE_GPMS_Provider (Prod) " + model.Firstname + " " + model.Lastname;
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                        }
                        else
                        {
                            transaction.Rollback();
                            _model.success = false;
                            _model.message = "(Prod) Doctor " + model.Firstname + " " + model.Lastname + " not added see logs";
                            //Logging.LogMessage(_model.message, LoggingPath);
                            _logger.Info(_model.message);
                            ErrorAudit(new Models.ErrorAudit
                            {
                                AuditUser = User.Identity.Name,
                                Error = _model.message,
                                ErrorTime = DateTime.Now
                            });
                            return _model;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _model.success = false;
                        _model.message = "Exception occurred while adding GE_CPMS_Provider: (Prod) " + model.Firstname + " " + model.Lastname + " " + ex.Message;
                        //Logging.LogMessage(_model.message, LoggingPath);
                        _logger.Info(_model.message);
                        ErrorAudit(new Models.ErrorAudit
                        {
                            AuditUser = User.Identity.Name,
                            Error = _model.message,
                            ErrorTime = DateTime.Now
                        });
                        return _model;
                    }
                }
                transaction.Commit(); // only commit if no errors occurred
                return _model;
            }
        }
        public IActionResult InsertDoctorRecord(Doctors model)
        {
            bool isError = false;
            try
            {
                if (DeployedEnv == "Dev")
                {
                    var success = AddDoctorDev(model);

                    if (success.success)
                    {
                        if (success.existMessage != null)
                        {
                            errors.Add(success.existMessage+Environment.NewLine);
                            isError = true;
                        }
                    }
                    else
                    {
                        //Logging.LogMessage(success.message, LoggingPath);
                        //_logger.Info(success.message, LoggingPath);
                        errors.Add(success.message + Environment.NewLine);
                        return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Errors Found", errors = errors });
                    }
                }
                else
                {
                    var success = AddDoctorDev(model);

                    if (success.success)
                    {
                        if (success.existMessage != null)
                        { 
                            errors.Add(success.existMessage + Environment.NewLine);
                            //_logger.Info(success.existMessage, LoggingPath);
                            isError = true;
                        }
                        success = AddDoctorProd(model);
                        if (success.success)
                        {
                            if (success.existMessage != null)
                            { 
                                errors.Add(success.existMessage + Environment.NewLine);
                                //_logger.Info(success.existMessage, LoggingPath);
                                isError = true;
                            }
                        }
                        else
                        {
                            errors.Add(success.message + Environment.NewLine);
                            //Logging.LogMessage(success.message, LoggingPath);
                            //_logger.Info(success.message, LoggingPath);
                            return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Errors Found", errors = errors });
                        }
                    }
                    else
                    {
                        errors.Add(success.message);
                        //Logging.LogMessage(success.message, LoggingPath);
                        //_logger.Info(success.message, LoggingPath);
                        return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Errors Found", errors = errors });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Info("Unknown Error: " + ex.Message);
                return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Unknown Error: " + ex.Message });
            }
            if (isError)
            {
                _logger.Info("Errors Found: " + errors);
                return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Errors Found", errors = errors });
            }
            //return RedirectToAction("Index", "Home");
            _logger.Info("Inserted into Doctors Table ");
            return RedirectToAction("Index", "Home", new SuccessModel { SuccessMessage = "Inserted into Doctors Table " });
        }
        public IActionResult InsertGPSMProviderRecord(GE_GPMS_Providers model)
        {
            bool isError = false;
            try
            {
                if (DeployedEnv == "Dev")
                {
                    var success = AddGPSMProviderDev(model);

                    if (success.success)
                    {
                        if (success.existMessage != null)
                        {
                            isError = true;
                            errors.Add(success.existMessage + Environment.NewLine);
                            _logger.Info(success.existMessage);
                        }
                    }
                    else
                    {
                        //Logging.LogMessage(success.message, LoggingPath);
                        _logger.Info(success.message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                        isError = true;
                        errors.Add(success.message + Environment.NewLine);
                    }
                }
                else
                {
                    var success = AddGPSMProviderDev(model);

                    if (success.success)
                    {
                        if (success.existMessage != null)
                        {
                            isError = true;
                            errors.Add(success.existMessage + Environment.NewLine);
                        }
                        success = AddGPSMProviderProd(model);
                        if (success.success)
                        {
                            if (success.existMessage != null)
                            {
                                isError = true;
                                errors.Add(success.existMessage + Environment.NewLine);
                            }
                        }
                        else
                        {
                            //Logging.LogMessage(success.message, LoggingPath);
                            _logger.Info(success.message);
                            ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                            isError = true;
                            errors.Add(success.message + Environment.NewLine);
                        }
                    }
                    else
                    {
                        //Logging.LogMessage(success.message, LoggingPath);
                        _logger.Info(success.message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                        isError = true;
                        errors.Add(success.message + Environment.NewLine);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Info("Unknown Error: " + ex.Message);
                return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Unknown Error: " + ex.Message });
            }
            if (isError)
            {
                _logger.Info("Errors Found: " + errors);
                return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Errors Found", errors = errors });
            }
            //return RedirectToAction("Index", "Home");
            _logger.Info("Inserted into GPSM Providers Table");
            return RedirectToAction("Index", "Home", new SuccessModel { SuccessMessage = "Inserted into GPSM Providers Table" });

        }
        public IActionResult InsertTouchworksRecord(TouchworksProviders model)
        {
            bool isError = false;
            try
            {
                var name = HttpContext.User.Identity.Name;
                if (model.DeaExp < new DateTime(1900, 01, 01))
                {
                    model.DeaExp = new DateTime(1900, 01, 01);
                }
                if (model.StateLicenseExp < new DateTime(1900, 01, 01))
                {
                    model.StateLicenseExp = new DateTime(1900, 01, 01);
                }
                if (model.LastLogin < new DateTime(1900, 01, 01))
                {
                    model.LastLogin = new DateTime(1900, 01, 01);
                }

                if (DeployedEnv == "Dev")
                {
                    var success = AddTouchworksDev(model);

                    if (success.success)
                    {
                        if (success.existMessage != null)
                        {
                            errors.Add(success.existMessage + Environment.NewLine);
                            isError = true;
                        }
                    }
                    else
                    {
                        // Logging.LogMessage(success.message, LoggingPath);
                        _logger.Info(success.message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                        errors.Add(success.message + Environment.NewLine);
                        isError = true;
                    }
                }
                else
                {
                    var success = AddTouchworksDev(model);

                    if (success.success)
                    {
                        if (success.existMessage != null)
                        {
                            errors.Add(success.existMessage + Environment.NewLine);
                            isError = true;
                        }
                        success = AddTouchworksProd(model);
                        if (success.success)
                        {
                            if (success.existMessage != null)
                            {
                                errors.Add(success.existMessage + Environment.NewLine);
                                isError = true;
                            }
                        }
                        else
                        {
                            //Logging.LogMessage(success.message, LoggingPath);
                            _logger.Info(success.message);
                            ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                            errors.Add(success.message + Environment.NewLine);
                            isError = true;
                        }
                    }
                    else
                    {
                        //Logging.LogMessage(success.message, LoggingPath);
                        _logger.Info(success.message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                        errors.Add(success.message + Environment.NewLine);
                        isError = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Info(ex.Message);
                return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = ex.Message });
            }
            if (isError)
            {
                _logger.Info("Errors Found: " + errors);
                return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Errors Found", errors = errors }) ;
            }
            _logger.Info("Inserted into Touchworks Providers Table");

            return RedirectToAction("Index", "Home", new SuccessModel { SuccessMessage = "Inserted into Touchworks Providers Table" });

        }
        // Excel Upload Logic
        [Authorize]
        public IActionResult UploadExcel()
        {
            return View();
        }
        [ActionName("UploadExcel")]
        [HttpPost]
        [Authorize]
        public IActionResult UploadExcel(IFormFile file)
        {
            string name = HttpContext.User.Identity.Name;

            ExcelModel models = new ExcelModel();
            models.GroupedModel = new List<GroupedModel>();

            if (file.Length > 0)
            {

                int i = 2;

                ExcelPackage xlPackage = new ExcelPackage(file.OpenReadStream());
                while (xlPackage.Workbook.Worksheets[0].Cells[i, 2].Value == null)
                {
                    i++;
                }

                while (xlPackage.Workbook.Worksheets[0].Cells[i, 2].Value != null)
                {
                    //Update Model with Values
                    if (!String.IsNullOrWhiteSpace(xlPackage.Workbook.Worksheets[0].Cells[i, 2].Value.ToString()))
                    {
                        try
                        {
                            models.GroupedModel.Add(new GroupedModel()
                            {
                                doctors = new Doctors()
                                {
                                    ChangeNotes = xlPackage.Workbook.Worksheets[0].Cells[i, 25] == null || xlPackage.Workbook.Worksheets[0].Cells[i, 25].Value == null ? DateTime.Now.ToShortDateString() + ", " + name + ", " + "" : DateTime.Now.ToShortDateString() + ", " + name + ", " + xlPackage.Workbook.Worksheets[0].Cells[i, 25].Value.ToString() + DateTime.Now.ToShortDateString() + " " + name,
                                    PhysicianGroup = xlPackage.Workbook.Worksheets[0].Cells[i, 10].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 10].Value.ToString(),
                                    ProviderActive = "Y",
                                    ProviderName = xlPackage.Workbook.Worksheets[0].Cells[i, 7].Value.ToString() + ", " + xlPackage.Workbook.Worksheets[0].Cells[i, 8].Value.ToString(),
                                    ProviderNpi = xlPackage.Workbook.Worksheets[0].Cells[i, 3].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 3].Value.ToString(),
                                    ProviderPasnbr = Decimal.Parse(xlPackage.Workbook.Worksheets[0].Cells[i, 5].Value == null ? "0" : xlPackage.Workbook.Worksheets[0].Cells[i, 5].Value.ToString()),
                                    ProviderUserId = ""
                                },
                                gE_GPMS = new GE_GPMS_Providers()
                                {
                                    Firstname = xlPackage.Workbook.Worksheets[0].Cells[i, 8].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 8].Value.ToString(),
                                    Lastname = xlPackage.Workbook.Worksheets[0].Cells[i, 7].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 7].Value.ToString(),
                                    GPMSID = xlPackage.Workbook.Worksheets[0].Cells[i, 4].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 4].Value.ToString(),
                                    TouchworksID = xlPackage.Workbook.Worksheets[0].Cells[i, 2].Value == null ? "0" : xlPackage.Workbook.Worksheets[0].Cells[i, 2].Value.ToString()
                                },
                                touchworks = new TouchworksProviders()
                                {
                                    Active = "Y",
                                    BillingProvider = xlPackage.Workbook.Worksheets[0].Cells[i, 22].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 22].Value.ToString().ToUpper(),
                                    Credentials = xlPackage.Workbook.Worksheets[0].Cells[i, 20].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 20].Value.ToString(),
                                    Dea = xlPackage.Workbook.Worksheets[0].Cells[i, 16].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 16].Value.ToString(),
                                    DeaExp = DateTime.Parse(xlPackage.Workbook.Worksheets[0].Cells[i, 17].Value == null ? "1900-01-01" : xlPackage.Workbook.Worksheets[0].Cells[i, 17].Value.ToString()),
                                    DefaultLocation = xlPackage.Workbook.Worksheets[0].Cells[i, 18].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 18].Value.ToString(),
                                    DefaultOrg = xlPackage.Workbook.Worksheets[0].Cells[i, 10].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 10].Value.ToString(),
                                    DirectId = xlPackage.Workbook.Worksheets[0].Cells[i, 24].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 24].Value.ToString(),
                                    FirstName = xlPackage.Workbook.Worksheets[0].Cells[i, 8].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 8].Value.ToString(),
                                    LastName = xlPackage.Workbook.Worksheets[0].Cells[i, 7].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 7].Value.ToString(),
                                    LastLogin = new DateTime(1900, 01, 01),
                                    Notes = xlPackage.Workbook.Worksheets[0].Cells[i, 25] == null || xlPackage.Workbook.Worksheets[0].Cells[i, 25].Value == null ? DateTime.Now.ToShortDateString() + ", " + name + ", " + "" : DateTime.Now.ToShortDateString() + ", " + name + ", " + xlPackage.Workbook.Worksheets[0].Cells[i, 25].Value.ToString(),
                                    Npi = xlPackage.Workbook.Worksheets[0].Cells[i, 3].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 3].Value.ToString().ToUpper(),
                                    OrgId = xlPackage.Workbook.Worksheets[0].Cells[i, 12].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 12].Value.ToString(),
                                    OrgUserHasAccessTo = xlPackage.Workbook.Worksheets[0].Cells[i, 11].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 11].Value.ToString().ToUpper(),
                                    Pas = xlPackage.Workbook.Worksheets[0].Cells[i, 5].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 5].Value.ToString(),
                                    Profession = xlPackage.Workbook.Worksheets[0].Cells[i, 19].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 19].Value.ToString(),
                                    ProviderCode = xlPackage.Workbook.Worksheets[0].Cells[i, 4].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 4].Value.ToString(),
                                    Schedulable = xlPackage.Workbook.Worksheets[0].Cells[i, 23].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 23].Value.ToString(),
                                    Specialty = xlPackage.Workbook.Worksheets[0].Cells[i, 21].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 21].Value.ToString(),
                                    SqlPersonId = int.Parse(xlPackage.Workbook.Worksheets[0].Cells[i, 2].Value == null ? "0" : xlPackage.Workbook.Worksheets[0].Cells[i, 2].Value.ToString()),
                                    StateLicense = xlPackage.Workbook.Worksheets[0].Cells[i, 13].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 13].Value.ToString().ToUpper(),
                                    StateLicenseExp = DateTime.Parse(xlPackage.Workbook.Worksheets[0].Cells[i, 15].Value == null ? "1900-01-01" : xlPackage.Workbook.Worksheets[0].Cells[i, 15].Value.ToString()),
                                    StateOfLicense = xlPackage.Workbook.Worksheets[0].Cells[i, 14].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 14].Value.ToString(),
                                    UserType = xlPackage.Workbook.Worksheets[0].Cells[i, 6].Value == null ? "" : xlPackage.Workbook.Worksheets[0].Cells[i, 6].Value.ToString()
                                }
                            });
                        }
                        catch (Exception e)
                        {
                            //_logger.LogError("Unexpected Error Occurred parsing Excel Spreadsheet " + e.Message);
                            _logger.Info("Unexpected Error Occurred parsing Excel Spreadsheet " + e.Message);

                            errors.Add(e.Message + Environment.NewLine);

                            return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Unexpected Error Occurred parsing Excel Spreadsheet", errors = errors });
                            //return RedirectToAction("SubmissionError", new { errorMessage = e.Message });

                        }
                    }
                    i++;
                }
                //models.toDev = true;

            }

            return View(models);
        }
        //Multiple Records Insert For Excel
        [Authorize]
        [HttpPost]
        public IActionResult InsertRecords(ExcelModel model)
        {
            int numDoctorRecordsDev = 0;
            int numGPMSRecordsDev = 0;
            int numTouchRecordsDev = 0;
            int numDoctorRecordsProd = 0;
            int numGPMSRecordsProd = 0;
            int numTouchRecordsProd = 0;
            int recordLine = 0;
            bool error = false;
            try
            {
                foreach (var groupedModel in model.GroupedModel)
                {
                    recordLine++;

                    try
                    {
                        if (DeployedEnv == "Dev")
                        {
                            var success = AddDoctorDev(groupedModel.doctors);

                            if (success.success)
                            {
                                if (success.existMessage == null)
                                {
                                    numDoctorRecordsDev++; // increment the doctors added number.
                                }
                                else
                                {
                                    error = true;
                                    errors.Add(success.existMessage );
                                }
                            }
                            else
                            {
                                //Logging.LogMessage(success.message, LoggingPath);
                                _logger.Info(success.message);
                                ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                                    Error = success.message, ErrorTime = DateTime.Now });
                                error = true;
                                errors.Add(success.message + Environment.NewLine);
                            }
                        }
                        else
                        {
                            var success = AddDoctorDev(groupedModel.doctors);

                            if (success.success)
                            {
                                if (success.existMessage == null)
                                {
                                    numDoctorRecordsDev++; // increment the doctors added number.
                                }
                                else
                                {
                                    error = true;
                                    errors.Add(success.existMessage + Environment.NewLine);
                                }
                                success = AddDoctorProd(groupedModel.doctors);
                                if (success.success)
                                {
                                    if (success.existMessage == null)
                                    {
                                        numDoctorRecordsProd++; // increment the doctors added number.
                                    }
                                    else
                                    {
                                        error = true;
                                        errors.Add(success.existMessage + Environment.NewLine);
                                    }
                                }
                                else
                                {
                                    //Logging.LogMessage(success.message, LoggingPath);
                                    _logger.Info(success.message);
                                    ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                    error = true;
                                    errors.Add(success.message + Environment.NewLine);
                                }
                            }
                            else
                            {
                                //Logging.LogMessage(success.message, LoggingPath);
                                _logger.Info(success.message);
                                ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                error = true;
                                errors.Add(success.message + Environment.NewLine);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        error = true;
                        errors.Add("Exception occurred adding Doctor " + groupedModel.doctors.ProviderName + " " + ex.Message + Environment.NewLine);
                        _logger.Info("Exception occurred adding Doctor " + groupedModel.doctors.ProviderName + " " + ex.Message);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = "Exception occurred adding Doctor " + groupedModel.doctors.ProviderName + " " + ex.Message, ErrorTime = DateTime.Now });
                    }

                    try
                    {
                        if (DeployedEnv == "Dev")
                        {
                            var success = AddTouchworksDev(groupedModel.touchworks);

                            if (success.success)
                            {
                                if (success.existMessage == null)
                                {
                                    numTouchRecordsDev++;
                                }
                                else
                                {
                                    error = true;
                                    _logger.Info(success.existMessage);
                                    errors.Add(success.existMessage + Environment.NewLine);
                                }
                            }
                            else
                            {
                                //Logging.LogMessage(success.message, LoggingPath);
                                _logger.Info(success.message);
                                ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                error = true;
                                errors.Add(success.message + Environment.NewLine);
                            }
                        }
                        else
                        {
                            var success = AddTouchworksDev(groupedModel.touchworks);

                            if (success.success)
                            {
                                if (success.existMessage == null)
                                {
                                    numTouchRecordsDev++;
                                }
                                else
                                {
                                    error = true;
                                    _logger.Info(success.existMessage);
                                    errors.Add(success.existMessage + Environment.NewLine);
                                }
                                success = AddTouchworksProd(groupedModel.touchworks);
                                if (success.success)
                                {
                                    if (success.existMessage == null)
                                    {
                                        numTouchRecordsProd++;
                                    }
                                    else
                                    {
                                        error = true;
                                        _logger.Info(success.existMessage);
                                        errors.Add(success.existMessage + Environment.NewLine);
                                    }
                                }
                                else
                                {
                                    //Logging.LogMessage(success.message, LoggingPath);
                                    _logger.Info(success.message);
                                    ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                    error = true;
                                    errors.Add(success.message + Environment.NewLine);
                                }
                            }
                            else
                            {
                                //Logging.LogMessage(success.message, LoggingPath);
                                _logger.Info(success.message);
                                ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                error = true;
                                errors.Add(success.message + Environment.NewLine);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        error = true;
                        _logger.Info("Exception occurred adding Touchworks Providers " + ex.Message);
                        errors.Append("Exception occurred adding Touchworks Providers " + ex.Message + Environment.NewLine);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = "Exception occurred adding Touchworks Providers " + ex.Message, ErrorTime = DateTime.Now });
                    }
                    try {
                        if (DeployedEnv == "Dev")
                        {
                            var success = AddGPSMProviderDev(groupedModel.gE_GPMS);

                            if (success.success)
                            {
                                if (success.existMessage == null)
                                {
                                    numGPMSRecordsDev++;
                                }
                                else
                                {
                                    error = true;
                                    _logger.Info(success.existMessage);
                                    errors.Add(success.existMessage + Environment.NewLine);
                                }
                            }
                            else
                            {
                                //Logging.LogMessage(success.message, LoggingPath);
                                _logger.Info(success.message);
                                ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                error = true;
                                errors.Add(success.message + Environment.NewLine);
                            }
                        }
                        else
                        {
                            var success = AddGPSMProviderDev(groupedModel.gE_GPMS);

                            if (success.success)
                            {
                                if (success.existMessage == null)
                                {
                                    numGPMSRecordsDev++;
                                }
                                else
                                {
                                    error = true;
                                    _logger.Info(success.existMessage);
                                    errors.Add(success.existMessage + Environment.NewLine);
                                }
                                success = AddGPSMProviderProd(groupedModel.gE_GPMS);
                                if (success.success)
                                {
                                    if (success.existMessage == null)
                                    {
                                        numGPMSRecordsProd++;
                                    }
                                    else
                                    {
                                        error = true;
                                        _logger.Info(success.existMessage);
                                        errors.Add(success.existMessage + Environment.NewLine);
                                    }
                                }
                                else
                                {
                                    //Logging.LogMessage(success.message, LoggingPath);
                                    _logger.Info(success.message);
                                    ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                    error = true;
                                    errors.Add(success.message + Environment.NewLine);
                                }
                            }
                            else
                            {
                                //Logging.LogMessage(success.message, LoggingPath);
                                _logger.Info(success.message);
                                ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, Error = success.message, ErrorTime = DateTime.Now });
                                error = true;
                                errors.Add(success.message + Environment.NewLine);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        error = true;
                        _logger.Info("An exception occurred adding GPSM Providers " + ex.Message);
                        errors.Add("An exception occurred adding GPSM Providers " + ex.Message + Environment.NewLine);
                        ErrorAudit(new Models.ErrorAudit { AuditUser = User.Identity.Name, 
                            Error = "An exception occurred adding GPSM Providers " + ex.Message, 
                            ErrorTime = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                errors.Append(ex.Message + Environment.NewLine);
                _logger.Info("Exception Occurred: " + ex.Message);
               return RedirectToAction("Error", "Integrations", new ErrorViewModel { exOutputMessage = "Exception Occurred " + ex.Message,
                   NumDoctorRecordsDev = numDoctorRecordsDev,
                   NumGPMSRecordsDev = numGPMSRecordsDev,
                   NumTouchworksRecordsDev = numTouchRecordsDev,
                   NumDoctorRecordsProd = numDoctorRecordsProd,
                   NumGPMSRecordsProd = numGPMSRecordsProd,
                   NumTouchworksRecordsProd = numTouchRecordsProd,
               });
            }
            if (error)
            {
                _logger.Info("Exception Occurred: " + errors);
                return RedirectToAction("Error", "Integrations", new ErrorViewModel
                {
                    exOutputMessage = "Errors Occurred" ,
                    errors = errors,
                    NumDoctorRecordsDev = numDoctorRecordsDev,
                    NumGPMSRecordsDev = numGPMSRecordsDev,
                    NumTouchworksRecordsDev = numTouchRecordsDev,
                    NumDoctorRecordsProd = numDoctorRecordsProd,
                    NumGPMSRecordsProd = numGPMSRecordsProd,
                    NumTouchworksRecordsProd = numTouchRecordsProd,
                });
            }

            else
            {
                _logger.Info("Inserted from Excel");
                return RedirectToAction("Index", "Home", new SuccessModel
                {
                    NumDoctorRecordsDev = numDoctorRecordsDev,
                    NumGPMSRecordsDev = numGPMSRecordsDev,
                    NumTouchworksRecordsDev = numTouchRecordsDev,
                    NumDoctorRecordsProd = numDoctorRecordsProd,
                    NumGPMSRecordsProd = numGPMSRecordsProd,
                    NumTouchworksRecordsProd = numTouchRecordsProd,
                    SuccessMessage = "Inserted from Excel"
                });
            }
            
        }        
    }
}