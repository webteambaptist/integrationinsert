﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationsInsert.Utils
{
    public class Logging 
    {
        public static void LogMessage(string message, string LoggingPath)
        {
            try
            {
                var fileName = LoggingPath + DateTime.Today.ToString("yyyyMMdd") + ".txt";
                File.AppendAllText(fileName, message + Environment.NewLine);
            }
            catch (Exception e)
            {

            }
        }

    }
}
