﻿using System;
using IntegrationsInsert.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace IntegrationsInsert.data
{
    public partial class ProdContext : DbContext
    {
        public ProdContext()
        {
        }

        public ProdContext(DbContextOptions<ProdContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Connections> Connections { get; set; }
        public virtual DbSet<Doctors> Doctors { get; set; }
        public virtual DbSet<LabConfidentialCodes> LabConfidentialCodes { get; set; }
        public virtual DbSet<TouchworksProviders> TouchworksProviders { get; set; }
        public virtual DbSet<GE_GPMS_Providers> GE_GPMS_Providers { get; set; }

        // Unable to generate entity type for table 'dbo.GE-GPMS_Providers'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TouchworksProviders_Staging'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=iwaTest_SQL;Database=Integrations;Trusted_Connection=True;");
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Connections>(entity =>
            {
                entity.HasKey(e => e.ConnectionId);

                entity.ToTable("Connections", "Storage");

                entity.Property(e => e.ConnectionId).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Doctors>(entity =>
            {
                entity.HasKey(e => new { e.ProviderPasnbr, e.PhysicianGroup })
                    .HasName("doctors_pk");

                entity.ToTable("doctors", "dbo");

                entity.Property(e => e.ProviderPasnbr)
                    .HasColumnName("ProviderPASnbr")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PhysicianGroup)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ChangeNotes)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderActive)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderNpi)
                    .HasColumnName("ProviderNPI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderUserId)
                    .HasColumnName("ProviderUserID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LabConfidentialCodes>(entity =>
            {
                entity.HasKey(e => e.LabCode);

                entity.ToTable("LabConfidentialCodes", "dbo");

                entity.Property(e => e.LabCode)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.LabDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TouchworksProviders>(entity =>
            {
                entity.ToTable("TouchworksProviders", "dbo");

                entity.Property(e => e.TouchworksProvidersId).HasColumnName("TouchworksProvidersID");

                entity.Property(e => e.Active)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.BillingProvider)
                    .HasColumnName("Billing_Provider")
                    .HasMaxLength(1);

                entity.Property(e => e.Credentials).HasMaxLength(50);

                entity.Property(e => e.Dea)
                    .HasColumnName("DEA")
                    .HasMaxLength(50);

                entity.Property(e => e.DeaExp)
                    .HasColumnName("DEA_Exp")
                    .HasColumnType("datetime");

                entity.Property(e => e.DefaultLocation)
                    .HasColumnName("Default_Location")
                    .HasMaxLength(250);

                entity.Property(e => e.DefaultOrg)
                    .HasColumnName("Default_Org")
                    .HasMaxLength(50);

                entity.Property(e => e.DirectId)
                    .HasColumnName("Direct_ID")
                    .HasMaxLength(150);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastLogin)
                    .HasColumnName("Last_Login")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Notes).HasMaxLength(4000);

                entity.Property(e => e.Npi)
                    .HasColumnName("NPI")
                    .HasMaxLength(50);

                entity.Property(e => e.OrgId)
                    .HasColumnName("Org_ID")
                    .HasMaxLength(15);

                entity.Property(e => e.OrgUserHasAccessTo)
                    .HasColumnName("Org_User_Has_Access_To")
                    .HasMaxLength(50);

                entity.Property(e => e.Pas)
                    .HasColumnName("PAS")
                    .HasMaxLength(25);

                entity.Property(e => e.Profession).HasMaxLength(50);

                entity.Property(e => e.ProviderCode)
                    .HasColumnName("Provider_Code")
                    .HasMaxLength(25);

                entity.Property(e => e.Schedulable).HasMaxLength(1);

                entity.Property(e => e.Specialty).HasMaxLength(50);

                entity.Property(e => e.SqlPersonId).HasColumnName("SQL_Person_ID");

                entity.Property(e => e.StateLicense)
                    .HasColumnName("State_License")
                    .HasMaxLength(50);

                entity.Property(e => e.StateLicenseExp)
                    .HasColumnName("State_License_Exp")
                    .HasColumnType("datetime");

                entity.Property(e => e.StateOfLicense)
                    .HasColumnName("State_of_License")
                    .HasMaxLength(50);

                entity.Property(e => e.UserType)
                    .HasColumnName("User_Type")
                    .HasMaxLength(50);
            });
            modelBuilder.Entity<GE_GPMS_Providers>(entity =>
            {
                entity.ToTable("GE-GPMS_Providers", "dbo");
                entity.HasKey(e => e.TouchworksID);
                entity.Property(e => e.GPMSID)
                .HasColumnName("GPMSID")
                .HasMaxLength(50);

                entity.Property(e => e.TouchworksID)
                    .HasColumnName("TouchWorksID")
                    .HasMaxLength(50);

                entity.Property(e => e.Lastname)
                    .HasColumnName("Lastname")
                    .HasMaxLength(100);

                entity.Property(e => e.Firstname)
                    .HasColumnName("Firstname")
                    .HasMaxLength(50);

            });
        }
    }
}
