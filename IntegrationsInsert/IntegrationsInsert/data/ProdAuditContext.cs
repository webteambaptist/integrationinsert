﻿using System;
using IntegrationsInsert.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace IntegrationsInsert.data
{
    public partial class ProdAuditContext : DbContext
    {
        public ProdAuditContext()
        {
        }

        public ProdAuditContext(DbContextOptions<ProdAuditContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DoctorsAudit> DoctorsAudit { get; set; }
        public virtual DbSet<GeGpmsProviderAudit> GeGpmsProviderAudit { get; set; }
        public virtual DbSet<TouchworksProvidersAudit> TouchworksProvidersAudit { get; set; }
        public virtual DbSet<ErrorAudit> ErrorAudit { get; set; }
        // Unable to generate entity type for table 'dbo.Error_Audit'. Please see the warning messages.

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=iwaTest_SQL;Database=IntegrationsAudit;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<DoctorsAudit>(entity =>
            {
                entity.HasKey(e => e.RecId);

                entity.ToTable("doctors_Audit", "dbo");

                entity.Property(e => e.RecId).HasColumnName("RecID");

                entity.Property(e => e.AuditActivity)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AuditTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.AuditUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ChangeNotes)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PhysicianGroup)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderActive)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderNpi)
                    .HasColumnName("ProviderNPI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderPasnbr)
                    .HasColumnName("ProviderPASnbr")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.ProviderUserId)
                    .HasColumnName("ProviderUserID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GeGpmsProviderAudit>(entity =>
            {
                entity.HasKey(e => e.RecId);

                entity.ToTable("GE-GPMS_Provider_Audit", "dbo");

                entity.Property(e => e.RecId).HasColumnName("RecID");

                entity.Property(e => e.AuditActivity)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AuditTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.AuditUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpmsid)
                    .IsRequired()
                    .HasColumnName("GPMSID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TouchWorksId)
                    .HasColumnName("TouchWorksID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TouchworksProvidersAudit>(entity =>
            {
                entity.HasKey(e => e.RecId);

                entity.ToTable("TouchworksProviders_Audit", "dbo");

                entity.Property(e => e.RecId).HasColumnName("RecID");

                entity.Property(e => e.Active).HasMaxLength(1);

                entity.Property(e => e.AuditActivity)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AuditTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.AuditUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingProvider)
                    .HasColumnName("Billing_Provider")
                    .HasMaxLength(1);

                entity.Property(e => e.Credentials).HasMaxLength(50);

                entity.Property(e => e.Dea)
                    .HasColumnName("DEA")
                    .HasMaxLength(50);

                entity.Property(e => e.DeaExp)
                    .HasColumnName("DEA_Exp")
                    .HasColumnType("datetime");

                entity.Property(e => e.DefaultLocation)
                    .HasColumnName("Default_Location")
                    .HasMaxLength(250);

                entity.Property(e => e.DefaultOrg)
                    .HasColumnName("Default_Org")
                    .HasMaxLength(50);

                entity.Property(e => e.DirectId)
                    .HasColumnName("Direct_ID")
                    .HasMaxLength(150);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastLogin)
                    .HasColumnName("Last_Login")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Notes).HasMaxLength(4000);

                entity.Property(e => e.Npi)
                    .HasColumnName("NPI")
                    .HasMaxLength(50);

                entity.Property(e => e.OrgId)
                    .HasColumnName("Org_ID")
                    .HasMaxLength(15);

                entity.Property(e => e.OrgUserHasAccessTo)
                    .HasColumnName("Org_User_Has_Access_To")
                    .HasMaxLength(50);

                entity.Property(e => e.Pas)
                    .HasColumnName("PAS")
                    .HasMaxLength(25);

                entity.Property(e => e.Profession).HasMaxLength(50);

                entity.Property(e => e.ProviderCode)
                    .HasColumnName("Provider_Code")
                    .HasMaxLength(25);

                entity.Property(e => e.Schedulable).HasMaxLength(1);

                entity.Property(e => e.Specialty).HasMaxLength(50);

                entity.Property(e => e.SqlPersonId).HasColumnName("SQL_Person_ID");

                entity.Property(e => e.StateLicense)
                    .HasColumnName("State_License")
                    .HasMaxLength(50);

                entity.Property(e => e.StateLicenseExp)
                    .HasColumnName("State_License_Exp")
                    .HasColumnType("datetime");

                entity.Property(e => e.StateOfLicense)
                    .HasColumnName("State_of_License")
                    .HasMaxLength(50);

                entity.Property(e => e.TouchworksProvidersId).HasColumnName("TouchworksProvidersID");

                entity.Property(e => e.UserType)
                    .HasColumnName("User_Type")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ErrorAudit>(entity =>
            {
                entity.ToTable("Error_Audit", "dbo");

                entity.Property(e => e.ID).HasColumnName("ID");

                entity.Property(e => e.Error);

                entity.Property(e => e.AuditUser)
                    .HasMaxLength(50);

                entity.Property(e => e.ErrorTime).HasColumnType("datetime");
                
            });
        }
    }
}
